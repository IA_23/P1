# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """

    "*** YOUR CODE HERE ***"

    """
    *** IMPORTANT ***

    Some of the comments in this function are repeated in the other search
    algorithms. This is because similar approaches are used in all of them.
    """

    frontier = util.Stack()
    """
    set shall be used since it does not allow duplicate elements. This will
    act as a second check to see if a state has already been expanded, because
    no state should be expanded twice.

    Same applies for the rest of the search algorithms.
    """
    expanded = set()
    startState = problem.getStartState()
    """
    Each node is composed by the state (A.K.A position in this case),
    along all the actions taken to reach said state.
    """
    startNode = (startState, [])

    frontier.push(startNode)

    """
    The algorithm will keep expanding nodes until the frontier is empty or
    the goal state is found. If the frontier becames empty without finding
    the goal state, then the algorithm will return None, indicating that
    there is no solution to the problem.
    """
    while not frontier.isEmpty():
        currentState, actions = frontier.pop()

        if problem.isGoalState(currentState):
            return actions

        """
        If the current state has not been expanded yet, then it will be
        expanded and all of its successors will be added to the frontier.
        
        expanding a node means adding it to the expanded set and adding all
        of its successors to the frontier.

        The actions taken to reach the current state will not be considered
        when adding the state to the expanded set. This is because the same
        state can be reached by different paths, and the algorithm should
        not consider the path taken to reach the state, but the state itself.

        In the event that the current state has already been expanded, then
        the node is ignored and the algorithm will continue to the next node
        in the frontier.
        """
        if currentState not in expanded:
            expanded.add(currentState)
            for succState, succAction, _ in problem.getSuccessors(currentState):
                """
                This avoids to add a successor to the frontier if it has
                already been expanded.

                Actually this is not necessary since the algorithm will pop
                the node from the frontier and then check if it has been
                expanded, ignoring it if it has.

                However, this might help on reducing the number of nodes
                added to the frontier, saving some memory and time.
                """
                if succState in expanded:
                    continue
                """
                The new action will be composed by the actions taken to reach
                the current state plus the action taken to reach the successor.
                """
                newAction = actions + [succAction]
                newNode = (succState, newAction)
                frontier.push(newNode)

    return None

def depthLimitedDFS(problem: SearchProblem, depth_limit):
    """
    *** MODIFICACIÓN 1 ***
    """

    """
    *** IMPORTANT ***

    Some of the comments in this function are repeated in the other search
    algorithms. This is because similar approaches are used in all of them.
    """

    frontier = util.Stack()
    expanded = set()
    startState = problem.getStartState()
    """
    The depth is now another component of the node. The start node's depth
    is 0.
    """
    startNode = (startState, [], 0)

    frontier.push(startNode)

    while not frontier.isEmpty():
        """
        The depth is extracted from the node, along with the other data.

        The rest of the algorithm remains as previously described.
        """
        currentState, actions, depth = frontier.pop()

        if problem.isGoalState(currentState):
            """
            This counter is used for having a real representation of the
            current nodes expanded, since the counter takes the nodes from
            all iterations took.
            """
            print("Expanded nodes in the solution iteration: ", len(expanded))
            return actions
        
        """
        Not only the current state has not have been expanded yet,
        but its depth also should fall under the depth_limit.

        depth_limit is set by the caller.
        """
        if currentState not in expanded and depth < depth_limit:
            expanded.add(currentState)
            for succState, succAction, _ in problem.getSuccessors(currentState):
                if succState in expanded:
                    continue
                newAction = actions + [succAction]
                """
                Successors are a step below in the successors tree. So,
                its depth is incremented by one.
                """
                newDepth = depth + 1
                newNode = (succState, newAction, newDepth)
                frontier.push(newNode)

    """
    Reach this point means that no solution was found. This may happen
    either by reaching the depth_limit or not. However, in this exercise
    is assumed that the solution exists so the situation will be certainly
    an empty frontier by trying to expand nodes below the depth_limit.
    """
    return None

def iterativeDeepening(problem: SearchProblem):
    """
    *** MODIFICACIÓN 1 ***
    """

    """
    The main difference with the standard dfs is the amount of nodes expanded.

    This actually is an issue with the implementation of the whole environment,
    because once a problem is set, every node expanded is counted. Thus, expanded
    nodes from previous iterations are still present in the final count once
    the solution is found. In order to solve this, an isolate counter should be
    implemented. Actually, I have done it, counting the lenght of the expanded
    set once the solution was found.

    (ID stands for iterativeDeepening):
    - ID: Nodes expanded: 364 (60211 total)

    Whereas bfs and dfs are:
    - DFS: Nodes expanded: 390
    - BFS: Nodes expanded: 620

    DFS and BFS where similar in terms of time. Iterative in the other hand,
    was a bit slower for this big maze. All of them ending up finding a path with the same cost of 210.

    In the medium maze, they are differences between all of them:

    - ID: Nodes expanded: 193 (8580 total). Cost of 70.
    - DFS: Nodes expanded: 146. Cost of 130.
    - BFS: Nodes expanded: 269. Cost of 68.

    For the tiny maze, this are the results:

    - ID: Nodes expanded: 15 (64 total). Cost of 8.
    - DFS: Nodes expanded 15. Cost of 10.
    - BFS: Nodes expanded 15. Cost of 8.

    In both mazes, time was similar.

    This indeed confirms that this algorithm would be helpfull only if you
    are willing to expend some time on finding the depth of the solution, or if you
    know the depth of the solution beforehand, specially on big scenarios.

    It sits in an engineering compromise between path cost and total nodes expanded,
    with the handicap of the time in larger scenarios where the depth of the solution
    is unknown.
    """
    current_depth = 0
    actions = None

    while actions is None:
        actions = depthLimitedDFS(problem, current_depth)
        current_depth += 1
    
    return actions
    

def breadthFirstSearch(problem: SearchProblem):
    """Search the shallowest nodes in the search tree first."""

    "*** YOUR CODE HERE ***"

    """
    Using a queue instead of a stack will make the algorithm expand the
    shallowest nodes first, since the nodes will be expanded in the order
    they were added to the frontier. This is really the only difference
    between this algorithm and the depth first search algorithm.

    That is why this section is no commented at all, any relevant info could
    be found in the depth first search algorithm definition.
    """
    frontier = util.Queue()
    expanded = set()
    startState = problem.getStartState()
    startNode = (startState, [])

    frontier.push(startNode)

    while not frontier.isEmpty():
        currentState, actions = frontier.pop()

        if problem.isGoalState(currentState):
            return actions

        if currentState not in expanded:
            expanded.add(currentState)
            for succState, succAction, _ in problem.getSuccessors(currentState):
                if succState in expanded:
                    continue
                newAction = actions + [succAction]
                newNode = (succState, newAction)
                frontier.push(newNode)

    return None

def uniformCostSearch(problem: SearchProblem):
    """Search the node of least total cost first."""

    "*** YOUR CODE HERE ***"

    """
    In this algorithm, the frontier is a priority queue, and the nodes are
    ordered by their cost. This means that the nodes with the lowest cost
    will be expanded first.

    This priority queue is implemented in the util.py file, which was defined
    by the course staff. The priority queue will pop always the element with
    the lowest priority, which in this case is the node with the lowest cost.
    """
    frontier = util.PriorityQueue()
    expanded = set()
    startState = problem.getStartState()
    """
    For now on, the nodes will be composed by the state, the actions taken
    to reach said state, and the cost of the actions taken to reach said
    state.
    """
    startNode = (startState, [], 0)

    """
    The priority queue will be ordered by the cost of the nodes, so the
    start node will be added to the frontier with a cost of 0.
    """
    frontier.push(startNode, 0)

    while not frontier.isEmpty():
        """
        In the queue, the nodes are stored as follows:
            ((state, actions), cost)
        However, Python allows to unpack the tuple, so the node can be
        extracted as follows:
            (state, actions, cost)

        This is done as a neccesety for the priority queue to work, since
        it will pop each node as a tuple of said three elements, but one of them
        is the same value that is used to order the nodes in the queue.
        """
        currentState, actions, currentCost = frontier.pop()

        if problem.isGoalState(currentState):
            return actions

        if currentState not in expanded:
            expanded.add(currentState)
            for succState, succAction, succCost in problem.getSuccessors(currentState):
                if succState in expanded:
                    continue
                newAction = actions + [succAction]
                """
                The new cost will be the cost of the actions taken to reach
                the current state plus the cost of the action taken to reach
                the successor.
                """
                newCost = currentCost + succCost
                """
                The new node will be composed by the state, the actions taken
                to reach said state, and the cost of the actions taken to
                reach said state.
                """
                newNode = (succState, newAction, newCost)
                """
                The new node will be added to the frontier with a cost equal
                to the cost of the actions taken to reach said state.
                """
                frontier.push(newNode, newCost)

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem: SearchProblem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""

    "*** YOUR CODE HERE ***"

    """
    The principle here is the same as in the uniform cost search algorithm,
    but the cost of each node will be the cost of the actions taken to reach
    said node plus the heuristic value of the node.
    """
    frontier = util.PriorityQueue()
    expanded = set()
    startState = problem.getStartState()
    """
    The heuristic value of the start node will be calculated and added to
    the cost of the actions taken to reach said node."""
    heuristicCost = heuristic(startState, problem)
    startNode = (startState, [], 0)

    """
    Since the cost of reaching the start node is 0, the heuristic value of
    the start node will be the same as the cost of the actions taken to reach
    said node.
    """
    frontier.push(startNode, heuristicCost)

    while not frontier.isEmpty():
        currentState, actions, currentCost = frontier.pop()

        if problem.isGoalState(currentState):
            return actions

        if currentState not in expanded:
            expanded.add(currentState)
            for succState, succAction, succCost in problem.getSuccessors(currentState):
                if succState in expanded:
                    continue
                newAction = actions + [succAction]
                succCost += currentCost
                newNode = (succState, newAction, succCost)
                """
                The heuristic value of the successor node will be calculated
                and added to the cost of the actions taken to reach said node.
                
                This is the way on prioritizing the nodes in the queue. Those
                with the lowest cost + lowest heuristic value will be expanded
                first.
                """
                succCost += heuristic(succState, problem)
                frontier.push(newNode, succCost)


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
